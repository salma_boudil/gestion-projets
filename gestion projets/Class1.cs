﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace gestion_projets
{
    class ADO
    {
        public SqlConnection con = new SqlConnection();
        public SqlCommand cmd = new SqlCommand();
        public SqlDataReader dr;
        public SqlDataAdapter dap;
        public DataTable dt = new DataTable();
        public DataSet ds = new DataSet();
        public DataRow ligne;
        public SqlCommandBuilder cb;

        public void connecter()
        {
            con.ConnectionString = "Datasource = SALMA, InitialCatalogue= prjt,Integrated Security=false";
            if (con.State.Equals(ConnectionState.Closed) || con.State.Equals(ConnectionState.Broken))
            {
                con.Open();
            }
        }

        // declaration de la methode deconnecter
        public void DECONNECTER()
        {
            if (con.State == ConnectionState.Open)
            {

                con.Close();
            }
        }
    }
}
